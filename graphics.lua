

function LeoDrawRectangle(x, y, w, h, color, mode, rx, ry)

    mode = mode or "fill"
    rx = rx or 0
    ry = ry or 0

    love.graphics.setColor(color)
    love.graphics.rectangle(mode, x, y, w, h, rx, ry)

end


function LeoDrawText(txt, x, y, color, font)

    color = color or LEO_THEME.fontColor
    font = font or LEO_THEME.font12

    love.graphics.setColor(color)
    love.graphics.setFont(font)
    love.graphics.print(txt, x, y)

    --love.graphics.printf( coloredtext, x, y, wraplimit, alignmode, angle, sx, sy, ox, oy, kx, ky )

end


function LeoDrawBackground()

    if LEO_THEME.wallpaperImage then

        --love.graphics.draw(LEO_THEME.wallpaperImage, 0, 0, 0, love.graphics.getWidth() / LEO_THEME.wallpaperImage:getWidth(), love.graphics.getHeight() / LEO_THEME.wallpaperImage:getHeight())
        
        local bestRatio = math.max(love.graphics.getWidth() / LEO_THEME.wallpaperImage:getWidth(), love.graphics.getHeight() / LEO_THEME.wallpaperImage:getHeight())
        love.graphics.draw(LEO_THEME.wallpaperImage, 0, 0, 0, bestRatio, bestRatio)
        
        --print(bestRatio)
        --print (LEO_THEME.wallpaperImage:getWidth() .. "x" .. LEO_THEME.wallpaperImage:getHeight())
        --print (love.graphics.getWidth() .. "x" .. love.graphics.getHeight())

    else

        LeoDrawRectangle(0, 0, love.graphics.getWidth(), love.graphics.getHeight(), LEO_THEME.wallpaperColor)

    end

    LeoDrawRectangle(LEO_THEME.viewsWidth, 0, love.graphics.getWidth() - LEO_THEME.viewsWidth, LEO_THEME.topHeight, LEO_THEME.topBackgroundColor)
    LeoDrawRectangle(LEO_THEME.viewsWidth, LEO_THEME.topHeight, love.graphics.getWidth() - LEO_THEME.viewsWidth, LEO_THEME.paramsHeight, LEO_THEME.paramsBackgroundColor)

    LeoDrawRectangle(0, LEO_THEME.topHeight, LEO_THEME.viewsWidth, love.graphics.getHeight() - LEO_THEME.topHeight, LEO_THEME.viewsBackgroundColor)

    LeoDrawRectangle(0, 0, LEO_THEME.viewsWidth, LEO_THEME.topHeight, LEO_THEME.magicBackgroundColor)

end
