

-- convert an "object" to a string
function LeoDump(o)

    if type(o) == 'table' then
        local s = '{ '
        for k,v in pairs(o) do
           if type(k) ~= 'number' then k = '"'..k..'"' end
           s = s .. '['..k..'] = ' .. LeoDump(v) .. ',\n'
        end
        return s .. '} '
     else
        return tostring(o)
     end

end

-- display an "object"
function LeoDebug(o)
    
    print(LeoDump(o))

end


-- from http://lua-users.org/wiki/SplitJoin
-- explode a string using a separator
function string:split( inSplitPattern, outResults )

    if not outResults then
       outResults = { }
    end

    local theStart = 1
    local theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )

    while theSplitStart do
        
       local val = string.sub( self, theStart, theSplitStart-1 ) ----

       if val and val ~= "" then table.insert( outResults, val) end ----

       theStart = theSplitEnd + 1
       theSplitStart, theSplitEnd = string.find( self, inSplitPattern, theStart )
    end

    local val = string.sub( self, theStart ) ----
    if val and val ~= "" then table.insert( outResults,  val) end ----

    return outResults

 end


function LeoFileExist(path)
    
    local f = io.open(path,"r")
    if f~=nil then io.close(f) return true else return false end

end

