
-- exec a system command and return the output as a string
function LeoExec(cmd)

    local ret = ""

    f = assert (io.popen (cmd))

    for line in f:lines() do
        ret = ret .. line .. "\n"
    end

    f:close()

    return ret

end


-- let's go to desktop mode
function LeoDesktopMode()

    LeoExec("xprop -name LeoDE -f _NET_WM_WINDOW_TYPE 32a -set _NET_WM_WINDOW_TYPE _NET_WM_WINDOW_TYPE_DESKTOP")
    LeoExec("xprop -name LeoDE -f _NET_WM_DESKTOP 32c -set _NET_WM_DESKTOP 0")

end


-- call a gsettings command
function LeoGSettings(cmd)

    return string.gsub(string.gsub(LeoExec("gsettings " .. cmd), "\n", ""), "'", "")
end


-- get Gnome (or Budgie...) wallpaper path
function LeoGetGnomeWallpaper()

    local ret = ""

    local path = LeoGSettings("get org.gnome.desktop.background picture-uri")

    if string.find(path, "file://") then
        ret = string.sub(path, 8, string.len(path))
    end

    return ret

end


function LeoSystemNotif(txt)

    print(LeoExec("python '".. love.filesystem.getSource( ) .. "/tools/notif.py'"))

end



