
return {

    name = "leo",

    font12 = love.graphics.newFont(12),
    font22 = love.graphics.newFont(22),
    font44 = love.graphics.newFont(44),
    font72 = love.graphics.newFont(72),
    fontColor = {1, 1, 1, 1},

    topBackgroundColor = {1, 0, 0, 0.3},
    topTextColor = {1, 1, 1, 1},
    topHeight = 100,

    viewsBackgroundColor = {0, 1, 0, 0.3},
    viewsWidth = 100,

    paramsBackgroundColor = {0, 0, 1, 0.3},
    paramsHeight = 100,

    magicBackgroundColor = {1, 0, 1, 0.3},

    --wallpaperImage = love.graphics.newImage("/home/davinci/Pictures/wallpaper.png"),
    --wallpaperImage = LEO_FS:loadImage("/home/davinci/Pictures/wallpaper.png"),
    wallpaperImage = LEO_FS:loadImage(LeoGetGnomeWallpaper()),
    wallpaperColor = {0.5, 0.5, 0.5, 1}

}


--LEO_VIEWS = {
    
--}
