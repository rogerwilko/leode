
-- get GTK theme
function LeoGetGtkTheme()

    return LeoGSettings("get org.gnome.desktop.interface gtk-theme")

end

-- get icons theme
function LeoGetIconTheme()

    return LeoGSettings("get org.gnome.desktop.interface icon-theme")

end



-- get a list of all .desktop (apps) files
function LeoListDesktopFiles()
    return load('return { ' .. LeoExec('for app in /usr/share/applications/*.desktop ~/.local/share/applications/*.desktop; do echo "\'${app}\',"; done') .. ' }')()
end


-- parse a .desktop file and return a table
function LeoParseDesktopFile(path)

    --[[

     in [Desktop Entry] :

        Version=1.0
        Name=App
        GenericName=App for something
        GenericName[lang]=in other language
        Comment=Allow to do something
        Comment[lang]=in other language
        Exec=/usr/bin/app %u
        Icon=iconName
        Terminal=false
        Type=Application
        MimeType=mime1;mime2; etc
        StartupNotify=true
        StartupWMClass=App
        Categories=Cat1;Cat2;
        Keywords=cat;beer;internet;
        Actions=actionName1;actionName2;

        NoDisplay=true


    in [Desktop Action actionName1] :

        Name=New Window
        Name[lang]=in other language
        Exec=/usr/bin/app --someOption %u
    ]]



    local data = LEO_LIP.load(path); -- a .desktop is a .inish

    for k,v in pairs(data) do -- we reparse to make a;b;c; values a table : {"a", "b", "c"}

        for k2, v2 in pairs(v) do

            local val = tostring(v2)

            if k2 == "Icon" then -- full icon path (or nil when not found)
                
                local iconPath = LeoFindIcon(val)
                --local iconPath = LeoGetIconPath(val)
                if iconPath then data[k]["IconPath"] = iconPath end

            end

            if val:find(";") then

                data[k][k2] = val:split(";")

            end

        end

    end

    -- some more informations
    data["Leo"] = {
        DesktopPath = path -- initial file path
    }


    return data
end


-- find the icon for an app
-- looking in the theme icons, and then in other icon directories
function LeoFindIcon(iconName)

    --[[
        NEED TO BE IMPROVED
        - directly handle inherited themes (instead of searching in Papirus-Adapta, Papirus,... after "main theme")
        - no svg support in Love2D :/
        - too time-consuming
    ]]

    local iconThemePath = "/usr/share/icons/" .. LeoGetIconTheme() .. "/"

    local dirsToSearch = {

        iconThemePath,
        "/usr/share/icons/Papirus-Adapta/",
        "/usr/share/icons/Papirus/",
        "/usr/share/icons/hicolor/",
        "/usr/share/icons/Adwaita/",
        "/usr/share/icons/gnome/"
    }


    local dirs = {
    }

    for i = 1, #dirsToSearch do
        
            table.insert(dirs, dirsToSearch[i] .. "512x512/apps/")
            table.insert(dirs, dirsToSearch[i] .. "384x384/apps/")
            table.insert(dirs, dirsToSearch[i] .. "256x256/apps/")
            table.insert(dirs, dirsToSearch[i] .. "192x192/apps/")
            table.insert(dirs, dirsToSearch[i] .. "128x128/apps/")
            table.insert(dirs, dirsToSearch[i] .. "96x96/apps/")
            table.insert(dirs, dirsToSearch[i] .. "72x72/apps/")
            table.insert(dirs, dirsToSearch[i] .. "64x64/apps/")
            table.insert(dirs, dirsToSearch[i] .. "48x48/apps/")
            table.insert(dirs, dirsToSearch[i] .. "36x36/apps/")
            table.insert(dirs, dirsToSearch[i] .. "32x32/apps/")

    end

    table.insert(dirs, "/usr/share/icons/")
    table.insert(dirs, "/usr/share/pixmaps/")
    


    -- search for a .png or .xpm
    for i = 1,#dirs do

        local fileToTry = dirs[i] .. iconName
        --print(fileToTry)

        if LeoFileExist(fileToTry .. ".png") then
            return fileToTry .. ".png"
        elseif LeoFileExist(fileToTry .. ".xpm") then
            return fileToTry .. ".xpm"
        elseif LeoFileExist(fileToTry .. ".svg") then -- try to convert in a temp dir?
            return fileToTry .. ".svg"
        end

    end

    print("Can't find icon " .. iconName)

    --return nil
    return iconName

end


function LeoGetIconPath(iconName)

    local cmd = "python '".. love.filesystem.getSource( ) .. "/tools/getIconPath.py' '" .. iconName:gsub(" ", "\\ ") .. "'"

    local path = LeoExec(cmd)

    if path ~= "" then
        return path
    end

    return nil

end


-- return apps (and all their data) by category, in a big table
function LeoAppsByCategory(listDesktopFiles)

    local allByCat = {}

    for i = 1,#listDesktopFiles do

        local app = LeoParseDesktopFile(listDesktopFiles[i])

        local cat = app["Desktop Entry"]["Categories"]

        if type(cat) == "string" and cat:find(";") then -- bug with some files at loading?
            cat = cat:split(";")
        end

        local nd = app["Desktop Entry"]["NoDisplay"]

        if  cat and (nd == nil or nd == false) then -- only displayable apps
            
            for j = 1,#cat do

                local catName = cat[j]

                if allByCat[catName] then -- already apps in this category

                    table.insert(allByCat[catName], app)

                else

                    allByCat[catName] = { app }

                end

            end

        end

    end

    return allByCat

end
