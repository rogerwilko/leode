#!/bin/love

-- ***************************

require "libs..classic..classic"
require "libs..lovefs..lovefs..lovefs"
LEO_LIP = require "libs..LIP..LIP"
--LEO_LGI = require 'libs..lgi..lgi..build_leo..usr..local..share..lua..5.1..lgi'
--LEO_LGI = require "lgi"
--LEO_GIO = LEO_LGI.require('Gio')

require "utilities"
require "system"
require "apps"
require "graphics"

require "views..appsView"


require "bouchon"


-- ***************************

-- config for dev

LEO_THEME_NAME = "leo"

-- ***************************



--[[
TODO

remettre chargement auto dans update
optimiser chargement icons : faire une boucle dans un script python
    plutot que d'appeler un script a chaque fois


Modules :
    Apps
    Files
    Favorites
    Multi Search
    Admin
    Messagerie
    Espace Perso

]]




LEO_UPTIME = 0



function love.load()

    LEO_FS = lovefs()

    -- load theme
    LEO_THEME_PATH = "themes.." .. LEO_THEME_NAME
    LEO_THEME = require(LEO_THEME_PATH .. "..theme")


    LeoDesktopMode()

    --LeoSystemNotif("test :o")
    --print(LeoGetIconPath("firefox"))



    --local list = LeoListDesktopFiles()
    --local allByCat = LeoAppsByCategory(list)

    --LeoDebug(allByCat) -- NOPE


    --local network = allByCat["Network"]
    --for i = 1, #network do
     --   print(network[i]["Desktop Entry"]["Name"] .. ":" .. network[i]["Leo"]["DesktopPath"] )
    --end


    --[[]

    local found = 0
    local notFound = 0

    for i = 1,#list do

        local a = LeoParseDesktopFile(list[i])

        local ic = a["Desktop Entry"]["Icon"]
        local nd = a["Desktop Entry"]["NoDisplay"]
        --local name = a["Desktop Entry"]["Name"]

        if ic and ic ~= "" and (nd == nil or nd == false) then

            print("LOOKING FOR '" .. ic .. "' ( " .. a["Desktop Entry"]["Name"] .. " )")
            
            local path = LeoFindIcon(ic)

            if path then
                print(",,, FOUND ,,,  --> " .. path)
                found = found + 1
            else
                print("  *** NOT FOUND ***")
                notFound = notFound + 1
            end

        end

    end

    print("found : " .. found)
    print("not found : " .. notFound)
    --]]





    
 end


 function love.update(dt)

    LEO_UPTIME = LEO_UPTIME + dt

    if (not LEO_LIST_APPS) then --or (math.floor(LEO_UPTIME) % 30 == 9) then ----

        print("* UPDATING APPS INFOS")

        LEO_LIST_APPS = LeoListDesktopFiles()
        LEO_INFO_APPS = LeoAppsByCategory(LEO_LIST_APPS)

    end

 end



function love.draw(dt) ----

    love.graphics.clear()

    LeoDrawBackground()

    LeoDrawText("HILLOU WOURLD", 120, 40, LEO_THEME.topTextColor, LEO_THEME.font22)

end



function love.quit()

    print("BYE")
    
  end




-- ***************************

