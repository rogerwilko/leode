#!/usr/bin/python

import sys
import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk
from gi.repository import Gio


theme = Gtk.IconTheme.get_default()
icon = theme.lookup_icon(sys.argv[1], 128, 0)
if icon is not None:
    print(icon.get_filename())
